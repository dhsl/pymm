# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui

from app.Settings import settings

from gui.Dialog import Dialog
from gui.ScrapeMovie import ScrapeMovie


class MovieDetailsDialog(Dialog):
    def __init__(self, movie, parent=None):
        self.movie = movie
        QtGui.QDialog.__init__(self, parent)

        # window properties
        self.setWindowTitle('Movie Details - ' + movie.getInfo('title'))

        mainLayout = QtGui.QVBoxLayout()
        self.setLayout(mainLayout)

        # window layout
        # ---------------------------------------------------------------------
        tabs = QtGui.QTabWidget(self)

        buttonScrape = QtGui.QPushButton('Scrape')
        buttonBox = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok |
                                           QtGui.QDialogButtonBox.Cancel,
                                           accepted=self.on_accept,
                                           rejected=self.on_reject,
                                           helpRequested=self.on_action)
        buttonBox.addButton(buttonScrape, QtGui.QDialogButtonBox.HelpRole)

        mainLayout.addWidget(tabs)
        mainLayout.addWidget(buttonBox)

        # fonts
        fontLabel = QtGui.QFont()
        fontLabel.setPointSize(7)
        fontLabel.setWeight(QtGui.QFont.Bold)

        # credits buttons
        buttonCreditsElement = QtGui.QFrame()
        buttonCreditsLayout = QtGui.QHBoxLayout()
        buttonCreditsLayout.setSpacing(0)
        buttonCreditsLayout.setContentsMargins(0, 0, 0, 0)
        buttonCreditsElement.setLayout(buttonCreditsLayout)
        buttonCreditsAdd = QtGui.QPushButton('Add')
        buttonCreditsDel = QtGui.QPushButton('Delete')
        buttonCreditsAdd.clicked.connect(self.on_credits_add)
        buttonCreditsDel.clicked.connect(self.on_credits_del)
        buttonCreditsLayout.addWidget(buttonCreditsAdd)
        buttonCreditsLayout.addWidget(buttonCreditsDel)

        # actor buttons
        buttonActorElement = QtGui.QFrame()
        buttonActorLayout = QtGui.QHBoxLayout()
        buttonActorLayout.setSpacing(0)
        buttonActorLayout.setContentsMargins(0, 0, 0, 0)
        buttonActorElement.setLayout(buttonActorLayout)
        buttonActorAdd = QtGui.QPushButton('Add')
        buttonActorDel = QtGui.QPushButton('Delete')
        buttonActorAdd.clicked.connect(self.on_actor_add)
        buttonActorDel.clicked.connect(self.on_actor_del)
        buttonActorLayout.addWidget(buttonActorAdd)
        buttonActorLayout.addWidget(buttonActorDel)

        # elements
        self.items = {
            'title': [
                QtGui.QLabel('Title'),
                QtGui.QLineEdit()
            ],
            'originaltitle': [
                QtGui.QLabel('Original title'),
                QtGui.QLineEdit()
            ],
            'rating': [
                QtGui.QLabel('Rating'),
                QtGui.QLineEdit()
            ],
            'year': [
                QtGui.QLabel('Year'),
                QtGui.QLineEdit()
            ],
            'votes': [
                QtGui.QLabel('Votes'),
                QtGui.QLineEdit()
            ],
            'outline': [
                QtGui.QLabel('Outline'),
                QtGui.QTextEdit()
            ],
            'plot': [
                QtGui.QLabel('Plot'),
                QtGui.QTextEdit()
            ],
            'tagline': [
                QtGui.QLabel('Tagline'),
                QtGui.QLineEdit()
            ],
            'runtime': [
                QtGui.QLabel('Runtime (in minutes)'),
                QtGui.QLineEdit()
            ],
            'thumb': [
                QtGui.QLabel('Thumbnails'),
                QtGui.QLineEdit()
            ],
            'fanart': [
                QtGui.QLabel('Fanarts'),
                QtGui.QLineEdit()
            ],
            'mpaa': [
                QtGui.QLabel('MPAA Rating'),
                QtGui.QLineEdit()
            ],
            'id': [
                QtGui.QLabel('IMDb ID'),
                QtGui.QLineEdit()
            ],
            'genre': [
                QtGui.QLabel('Genre'),
                QtGui.QListWidget()
            ],
            'country': [
                QtGui.QLabel('Country'),
                QtGui.QLineEdit()
            ],
            'credits': [
                QtGui.QLabel('Credits'),
                QtGui.QListWidget(),
                buttonCreditsElement
            ],
            'director': [
                QtGui.QLabel('Director'),
                QtGui.QLineEdit()
            ],
            'premiered': [
                QtGui.QLabel('Premiered'),
                QtGui.QLineEdit()
            ],
            'status': [
                QtGui.QLabel('Status'),
                QtGui.QLineEdit()
            ],
            'code': [
                QtGui.QLabel('Code'),
                QtGui.QLineEdit()
            ],
            'aired': [
                QtGui.QLabel('aired'),
                QtGui.QLineEdit()
            ],
            'studio': [
                QtGui.QLabel('studio'),
                QtGui.QLineEdit()
            ],
            'trailer': [
                QtGui.QLabel('trailer'),
                QtGui.QLineEdit()
            ],
            'actor': [
                QtGui.QLabel('actor'),
                QtGui.QTreeWidget(),
                buttonActorElement
            ],
            'set': [
                QtGui.QLabel('Set'),
                QtGui.QLineEdit()
            ],
            # missing: fileinfo
        }

        elements = {}
        for title, items in self.items.items():
            elements[title] = QtGui.QFrame()
            layout = QtGui.QVBoxLayout()
            elements[title].setLayout(layout)

            layout.setSpacing(0)
            layout.setContentsMargins(0, 0, 0, 8)

            for item in items:
                layout.addWidget(item)
                if isinstance(item, QtGui.QLabel):
                    item.setFont(fontLabel)

        self.items['actor'][1].setHeaderLabels(['Name', 'Role', 'Thumb'])
        self.items['actor'][1].itemSelectionChanged.connect(self.on_actor_changed)

        # main tab
        # ---------------------------------------------------------------------
        tabMain = QtGui.QFrame(self)
        tabs.addTab(tabMain, 'Main')

        tabMainLayout = QtGui.QGridLayout()
        tabMainLayout.setVerticalSpacing(0)
        tabMain.setLayout(tabMainLayout)

        tabMainLayout.addWidget(elements['title'], 0, 0, 1, 2)
        tabMainLayout.addWidget(elements['tagline'], 1, 0, 1, 2)
        tabMainLayout.addWidget(elements['originaltitle'], 2, 0, 1, 2)
        tabMainLayout.addWidget(elements['year'], 3, 0, 1, 1)
        tabMainLayout.addWidget(elements['runtime'], 3, 1, 1, 1)
        tabMainLayout.addWidget(elements['rating'], 4, 0, 1, 1)
        tabMainLayout.addWidget(elements['votes'], 4, 1, 1, 1)
        tabMainLayout.addWidget(elements['director'], 5, 0, 1, 2)
        tabMainLayout.addWidget(elements['credits'], 6, 0, 1, 2)
        tabMainLayout.addWidget(elements['outline'], 0, 2, 4, 1)
        tabMainLayout.addWidget(elements['actor'], 4, 2, 3, 1)
        tabMainLayout.addWidget(elements['plot'], 0, 3, 4, 1)
        tabMainLayout.addWidget(elements['genre'], 4, 3, 3, 1)

        # poster tab
        # ---------------------------------------------------------------------
        tabPoster = QtGui.QFrame(self)
        tabs.addTab(tabPoster, 'Poster')

        # fanart tab
        # ---------------------------------------------------------------------
        tabFanart = QtGui.QFrame(self)
        tabs.addTab(tabFanart, 'Fanart')

        self.fillDialog()

    # Actions
    # -------------------------------------------------------------------------
    def on_accept(self):
        pass

    def on_reject(self):
        pass

    def on_action(self):
        d = ScrapeMovie(self.movie, self)
        d.exec_()

    def on_credits_add(self):
        self.listAdd('credits')

    def on_credits_del(self):
        self.listDel('credits')

    def on_actor_add(self):
        self.treeAdd('actor')

    def on_actor_del(self):
        self.items['actor'][1].takeTopLevelItem(self.selectedActor)

    def on_actor_changed(self):
        item = self.items['actor'][1].selectedItems()[0]
        self.selectedActor = self.items['actor'][1].indexFromItem(item).row()

    # Data handling
    # -------------------------------------------------------------------------
    def fillDialog(self):
        self.clearDialog()
        for key in ['title', 'originaltitle', 'rating', 'year', 'votes',
                    'outline', 'plot', 'tagline', 'runtime', 'mpaa', 'id',
                    'country', 'set', 'director', 'premiered', 'status',
                    'code', 'aired', 'studio', 'trailer']:
            self.items[key][1].setText(self.movie.getInfo(key))

        for credit in self.movie.getInfo('credits'):
            element = QtGui.QListWidgetItem(credit)
            element.setFlags(element.flags() | QtCore.Qt.ItemIsEditable)
            self.items['credits'][1].addItem(element)

        for genre in settings.config['general']['genres']:
            genre = unicode(genre)
            element = QtGui.QListWidgetItem(genre)
            element.setFlags(element.flags() | QtCore.Qt.ItemIsUserCheckable)
            if genre in self.movie.getInfo('genre'):
                element.setCheckState(2)
            else:
                element.setCheckState(0)
            self.items['genre'][1].addItem(element)

        for actor in self.movie.getInfo('actor'):
            name = unicode(actor['name'])
            role = unicode(actor['role'])
            thumb = unicode(actor['thumb'])
            element = QtGui.QTreeWidgetItem([name, role, thumb])
            element.setFlags(element.flags() | QtCore.Qt.ItemIsEditable)
            self.items['actor'][1].addTopLevelItem(element)
