# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui

from app.Settings import settings

from gui.Dialog import Dialog

from lib import tmdb3


class SettingsDialog(Dialog):
    def __init__(self, parent, main):
        QtGui.QDialog.__init__(self, parent)
        self.main = main

        # window properties
        self.setWindowTitle('Settings')

        mainLayout = QtGui.QVBoxLayout()
        self.setLayout(mainLayout)

        # window layout
        # ---------------------------------------------------------------------
        tabs = QtGui.QTabWidget(self)
        buttonBox = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok |
                                           QtGui.QDialogButtonBox.Cancel,
                                           accepted=self.on_accept,
                                           rejected=self.reject)

        mainLayout.addWidget(tabs)
        mainLayout.addWidget(buttonBox)

        # fonts
        fontLabel = QtGui.QFont()
        fontLabel.setPointSize(7)
        fontLabel.setWeight(QtGui.QFont.Bold)

        # movie sources buttons
        buttonMovieSourcesElement = QtGui.QFrame()
        buttonMovieSourcesLayout = QtGui.QHBoxLayout()
        buttonMovieSourcesLayout.setSpacing(0)
        buttonMovieSourcesLayout.setContentsMargins(0, 0, 0, 0)
        buttonMovieSourcesElement.setLayout(buttonMovieSourcesLayout)
        buttonMovieSourcesAdd = QtGui.QPushButton('Add')
        buttonMovieSourcesDel = QtGui.QPushButton('Delete')
        buttonMovieSourcesAdd.clicked.connect(self.on_movieSources_add)
        buttonMovieSourcesDel.clicked.connect(self.on_movieSources_del)
        buttonMovieSourcesLayout.addWidget(buttonMovieSourcesAdd)
        buttonMovieSourcesLayout.addWidget(buttonMovieSourcesDel)

        # movie sources buttons
        buttonSeriesSourcesElement = QtGui.QFrame()
        buttonSeriesSourcesLayout = QtGui.QHBoxLayout()
        buttonSeriesSourcesLayout.setSpacing(0)
        buttonSeriesSourcesLayout.setContentsMargins(0, 0, 0, 0)
        buttonSeriesSourcesElement.setLayout(buttonSeriesSourcesLayout)
        buttonSeriesSourcesAdd = QtGui.QPushButton('Add')
        buttonSeriesSourcesDel = QtGui.QPushButton('Delete')
        buttonSeriesSourcesAdd.clicked.connect(self.on_seriesSources_add)
        buttonSeriesSourcesDel.clicked.connect(self.on_seriesSources_del)
        buttonSeriesSourcesLayout.addWidget(buttonSeriesSourcesAdd)
        buttonSeriesSourcesLayout.addWidget(buttonSeriesSourcesDel)

        # genres buttons
        buttonGenresElement = QtGui.QFrame()
        buttonGenresLayout = QtGui.QHBoxLayout()
        buttonGenresLayout.setSpacing(0)
        buttonGenresLayout.setContentsMargins(0, 0, 0, 0)
        buttonGenresElement.setLayout(buttonGenresLayout)
        buttonGenresUpdate = QtGui.QPushButton('Update')
        buttonGenresUpdate.clicked.connect(self.on_genres_update)
        buttonGenresLayout.addWidget(buttonGenresUpdate)

        # elements
        self.items = {
            'movieSources': [
                QtGui.QLabel('Sources'),
                QtGui.QListWidget(),
                buttonMovieSourcesElement
            ],
            'seriesSources': [
                QtGui.QLabel('Sources'),
                QtGui.QListWidget(),
                buttonSeriesSourcesElement
            ],
            'genres': [
                QtGui.QLabel('Genres'),
                QtGui.QListWidget(),
                buttonGenresElement
            ],
        }

        elements = {}
        for title, items in self.items.items():
            elements[title] = QtGui.QFrame()
            layout = QtGui.QVBoxLayout()
            elements[title].setLayout(layout)

            layout.setSpacing(0)
            layout.setContentsMargins(0, 0, 0, 8)

            for item in items:
                layout.addWidget(item)
                if isinstance(item, QtGui.QLabel):
                    item.setFont(fontLabel)

        # general tab
        # ---------------------------------------------------------------------
        tabGeneral = QtGui.QFrame(self)
        tabs.addTab(tabGeneral, 'General')

        tabGeneralLayout = QtGui.QGridLayout()
        tabGeneralLayout.setVerticalSpacing(0)
        tabGeneral.setLayout(tabGeneralLayout)

        tabGeneralLayout.addWidget(elements['genres'], 0, 0, 1, 1)

        # movie tab
        # ---------------------------------------------------------------------
        tabMovies = QtGui.QFrame(self)
        tabs.addTab(tabMovies, 'Movies')

        tabMoviesLayout = QtGui.QGridLayout()
        tabMoviesLayout.setVerticalSpacing(0)
        tabMovies.setLayout(tabMoviesLayout)

        tabMoviesLayout.addWidget(elements['movieSources'], 0, 0, 1, 1)

        # series tab
        # ---------------------------------------------------------------------
        tabSeries = QtGui.QFrame(self)
        tabs.addTab(tabSeries, 'Series')

        tabSeriesLayout = QtGui.QGridLayout()
        tabSeriesLayout.setVerticalSpacing(0)
        tabSeries.setLayout(tabSeriesLayout)

        tabSeriesLayout.addWidget(elements['seriesSources'], 0, 0, 1, 1)

        settings.loadConfig()
        self.fillDialog()

    # Actions
    # -------------------------------------------------------------------------
    def on_accept(self):
        self.fillConfig()
        settings.saveConfig()
        self.accept()

    def on_movieSources_add(self):
        self.listAdd('movieSources')

    def on_seriesSources_add(self):
        self.listAdd('seriesSources')

    def on_movieSources_del(self):
        self.items['movieSources'][1].takeItem(self.items['movieSources'][1].currentRow())

    def on_seriesSources_del(self):
        self.items['seriesSources'][1].takeItem(self.items['seriesSources'][1].currentRow())

    def on_genres_update(self):
        self.items['genres'][1].clear()
        genreList = tmdb3.Genre().getAll()
        for genre in genreList:
            name = unicode(genre.name)
            element = QtGui.QListWidgetItem(name)
            self.items['genres'][1].addItem(element)

    # Data handling (FUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU)
    # -------------------------------------------------------------------------
    def fillDialog(self):
        self.clearDialog()
        for i in settings.config['movies']['sources']:
            element = QtGui.QListWidgetItem(i)
            element.setFlags(element.flags() | QtCore.Qt.ItemIsEditable)
            self.items['movieSources'][1].addItem(element)
        for i in settings.config['series']['sources']:
            element = QtGui.QListWidgetItem(i)
            element.setFlags(element.flags() | QtCore.Qt.ItemIsEditable)
            self.items['seriesSources'][1].addItem(element)
        for genre in settings.config['general']['genres']:
            element = QtGui.QListWidgetItem(genre)
            self.items['genres'][1].addItem(element)

    def fillConfig(self):
        settings.defaultConfig()
        for index in xrange(self.items['movieSources'][1].count()):
            settings.addSource('movies',
                               self.items['movieSources'][1].item(index).text())
        for index in xrange(self.items['seriesSources'][1].count()):
            settings.addSource('series',
                               self.items['seriesSources'][1].item(index).text())
        for index in xrange(self.items['genres'][1].count()):
            settings.addGenre(self.items['genres'][1].item(index).text())
