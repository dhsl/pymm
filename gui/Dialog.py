# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui


class Dialog(QtGui.QDialog):
    def listAdd(self, parent):
        element = QtGui.QListWidgetItem()
        element.setFlags(element.flags() | QtCore.Qt.ItemIsEditable)
        self.items[parent][1].addItem(element)
        self.items[parent][1].editItem(element)

    def listDel(self, parent):
        index = self.items[parent][1].currentRow()
        self.items[parent][1].takeItem(index)

    def treeAdd(self, parent):
        element = QtGui.QTreeWidgetItem()
        element.setFlags(element.flags() | QtCore.Qt.ItemIsEditable)
        self.items[parent][1].addTopLevelItem(element)
        self.items[parent][1].editItem(element)

    def clearDialog(self):
        for title, items in self.items.items():
            for item in items:
                if isinstance(item, (QtGui.QListWidget, QtGui.QTreeWidget)):
                    item.clear()
