# -*- coding: utf-8 -*-

import logging
import urllib
from PyQt4 import QtGui

from lib import tmdb3


class ScrapeMovie(QtGui.QDialog):
    def __init__(self, movie, parent=None):
        self.movie = movie
        QtGui.QDialog.__init__(self, parent)

        # window properties
        self.setWindowTitle('Scrape Movie')

        self.mainLayout = QtGui.QVBoxLayout()
        self.setLayout(self.mainLayout)

        # all pages
        # ---------------------------------------------------------------------
        self.createSearchMoviesPage()

        # add pages
        #self.addPage(self.createSearchMoviesPage())
        #self.addPage(self.createPosterPage())

    # pages
    # -------------------------------------------------------------------------
    def createSearchMoviesPage(self):
        page = QtGui.QFrame()
        pageLayout = QtGui.QHBoxLayout()
        page.setLayout(pageLayout)

        buttonBox = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok |
                                           QtGui.QDialogButtonBox.Cancel,
                                           accepted=self.on_accept_search,
                                           rejected=self.on_reject)

        title = self.movie.getInfo('title')
        year = self.movie.getInfo('year')
        self.res = tmdb3.searchMovie(title, year=year)
        moviesList = QtGui.QListWidget()
        for movie in self.res:
            element = QtGui.QListWidgetItem(movie.title)
            moviesList.addItem(element)
        moviesList.currentRowChanged.connect(self.on_moviesList_selected)

        # fonts
        fontLabel = QtGui.QFont()
        fontLabel.setPointSize(7)
        fontLabel.setWeight(QtGui.QFont.Bold)

        self.previewItems = {
            'title': [
                QtGui.QLabel('Title'),
                QtGui.QLineEdit()
            ],
            'originaltitle': [
                QtGui.QLabel('Original title'),
                QtGui.QLineEdit()
            ],
            'released': [
                QtGui.QLabel('Released'),
                QtGui.QLineEdit()
            ],
            'popularity': [
                QtGui.QLabel('Popularity'),
                QtGui.QLineEdit()
            ]
        }

        elements = {}
        for title, items in self.previewItems.items():
            elements[title] = QtGui.QFrame()
            layout = QtGui.QVBoxLayout()
            elements[title].setLayout(layout)

            layout.setSpacing(0)
            layout.setContentsMargins(0, 0, 0, 8)

            for item in items:
                layout.addWidget(item)
                if isinstance(item, QtGui.QLabel):
                    item.setFont(fontLabel)
                if isinstance(item, QtGui.QLineEdit):
                    item.setReadOnly(True)

        moviesPreview = QtGui.QFrame()
        moviesPreviewLayout = QtGui.QVBoxLayout()
        moviesPreviewLayout.setSpacing(0)
        moviesPreview.setLayout(moviesPreviewLayout)

        moviesPreviewLayout.addWidget(elements['title'])
        moviesPreviewLayout.addWidget(elements['originaltitle'])
        moviesPreviewLayout.addWidget(elements['released'])
        moviesPreviewLayout.addWidget(elements['popularity'])

        pageLayout.addWidget(moviesList)
        pageLayout.addWidget(moviesPreview)

        self.mainLayout.addWidget(page)
        self.mainLayout.addWidget(buttonBox)

    def createPosterPage(self):
        page = QtGui.QFrame()
        pageLayout = QtGui.QGridLayout()
        page.setLayout(pageLayout)

        buttonBox = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok |
                                           QtGui.QDialogButtonBox.Cancel,
                                           accepted=self.on_accept_poster,
                                           rejected=self.on_reject)

        items = []
        for poster in self.movie.posters:
            item = QtGui.QFrame()
            itemLayout = QtGui.QVBoxLayout()
            item.setLayout(itemLayout)
            item.setFrameShape(QtGui.QFrame.Box)
            logging.debug('Found poster %s (%sx%s)', poster.filename,
                          poster.width, poster.height)
            data = urllib.urlopen(poster.geturl(poster.sizes()[1])).read()
            pic = QtGui.QPixmap()
            pic.loadFromData(data)
            label = QtGui.QLabel('Image')
            label.setPixmap(pic)
            res = QtGui.QLabel(str(poster.width) + 'x' + str(poster.height))
            itemLayout.addWidget(label)
            itemLayout.addWidget(res)

            items.append(item)

        col = 0
        row = 0
        for item in items:
            pageLayout.addWidget(item, row, col)
            if col < 3:
                col = col + 1
            else:
                row = row + 1
                col = 0

        self.mainLayout.addWidget(page)
        self.mainLayout.addWidget(buttonBox)
        logging.error('TODO: implement selecting of posters')

    def createBackdropPage(self):
        page = QtGui.QFrame()
        pageLayout = QtGui.QGridLayout()
        page.setLayout(pageLayout)

        buttonBox = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok |
                                           QtGui.QDialogButtonBox.Cancel,
                                           accepted=self.on_accept_backdrop,
                                           rejected=self.on_reject)

        items = []
        for backdrop in self.movie.backdrops:
            item = QtGui.QFrame()
            itemLayout = QtGui.QVBoxLayout()
            item.setLayout(itemLayout)
            item.setFrameShape(QtGui.QFrame.Box)
            logging.debug('Found backdrop %s (%sx%s)', backdrop.filename,
                          backdrop.width, backdrop.height)
            data = urllib.urlopen(backdrop.geturl(backdrop.sizes()[1])).read()
            pic = QtGui.QPixmap()
            pic.loadFromData(data)
            label = QtGui.QLabel('Image')
            label.setPixmap(pic)
            res = QtGui.QLabel(str(backdrop.width) + 'x' + str(backdrop.height))
            itemLayout.addWidget(label)
            itemLayout.addWidget(res)
            items.append(item)

        col = 0
        row = 0
        for item in items:
            pageLayout.addWidget(item, row, col)
            if col < 3:
                col = col + 1
            else:
                row = row + 1
                col = 0

        self.mainLayout.addWidget(page)
        self.mainLayout.addWidget(buttonBox)
        logging.error('TODO: implement selecting of backdrop')

    # actions
    # -------------------------------------------------------------------------
    def on_accept_search(self):
        for i in range(self.mainLayout.count()):
            item = self.mainLayout.takeAt(0)
            item.widget().deleteLater()

        self.createPosterPage()

    def on_accept_poster(self):
        for i in range(self.mainLayout.count()):
            item = self.mainLayout.takeAt(0)
            item.widget().deleteLater()

        self.createBackdropPage()

    def on_accept_backdrop(self):
        for i in range(self.mainLayout.count()):
            item = self.mainLayout.takeAt(0)
            item.widget().deleteLater()

    def on_reject(self):
        pass

    def on_moviesList_selected(self, row):
        self.movie = self.res[row]
        self.previewItems['title'][1].setText(self.movie.title)
        self.previewItems['originaltitle'][1].setText(self.movie.originaltitle)
        self.previewItems['released'][1].setText(unicode(self.movie.releasedate))
        self.previewItems['popularity'][1].setText(unicode(self.movie.popularity))
