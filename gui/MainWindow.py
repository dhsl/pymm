# -*- coding: utf-8 -*-
"""main window - let there be dragons"""

import logging
from PyQt4 import QtCore, QtGui

from app.Worker import MoviesWorker
from app.Settings import settings

from gui.MovieDetailsDialog import MovieDetailsDialog
from gui.SettingsDialog import SettingsDialog

from lib import tmdb3


class MainWindow(QtGui.QMainWindow):
    """implements the main window"""
    def __init__(self, parent=None):
        self.movies = {}

        QtGui.QMainWindow.__init__(self)
        QtGui.QApplication.setStyle(QtGui.QStyleFactory.create('plastique'))

        # window properties
        self.setWindowTitle('pymm ' + settings.VERSION)

        # File Menu
        # ---------------------------------------------------------------------
        menuFile = self.menuBar().addMenu('File')
        menuFile.addAction('Settings', self.on_settings_dialog)
        menuFile.addSeparator()
        menuFile.addAction('Exit', QtGui.qApp.quit)

        # Box left
        # ---------------------------------------------------------------------
        # movies
        tabMoviesLayout = QtGui.QVBoxLayout()
        tabMovies = QtGui.QFrame(self)
        tabMovies.setLayout(tabMoviesLayout)

        actionMovieDetails = QtGui.QAction('Details', self)
        actionMovieDetails.triggered.connect(self.on_movieDetails)

        actionMovieScrape = QtGui.QAction('Scrape', self)
        actionMovieScrape.triggered.connect(self.on_movieScrape)

        self.listMovies = QtGui.QTreeWidget(self)
        self.listMovies.itemActivated.connect(self.on_movieDetails)
        self.listMovies.setContextMenuPolicy(QtCore.Qt.ActionsContextMenu)
        self.listMovies.addAction(actionMovieDetails)
        self.listMovies.addAction(actionMovieScrape)
        tabMoviesLayout.addWidget(self.listMovies)

        # series
        tabSeriesLayout = QtGui.QVBoxLayout()
        tabSeries = QtGui.QFrame(self)
        tabSeries.setLayout(tabSeriesLayout)

        actionSeriesDetails = QtGui.QAction('Details', self)
        actionSeriesDetails.triggered.connect(self.on_seriesDetails)

        listSeries = QtGui.QTreeWidget(self)
        listSeries.itemActivated.connect(self.on_seriesDetails)
        listSeries.setContextMenuPolicy(QtCore.Qt.ActionsContextMenu)
        listSeries.addAction(actionSeriesDetails)
        tabSeriesLayout.addWidget(listSeries)

        # glue
        boxLeft = QtGui.QTabWidget(self)
        boxLeft.addTab(tabMovies, 'Movies')
        boxLeft.addTab(tabSeries, 'Series')

        # Box right
        # ---------------------------------------------------------------------
        boxRight = QtGui.QFrame(self)

        # Main layout
        # ---------------------------------------------------------------------
        splitter = QtGui.QSplitter(QtCore.Qt.Horizontal)
        self.setCentralWidget(splitter)

        splitter.addWidget(boxLeft)
        splitter.addWidget(boxRight)

        self.on_refresh()

    # Dialogs
    # -------------------------------------------------------------------------
    def on_settings_dialog(self):
        d = SettingsDialog(self, self)
        if d.exec_():
            self.on_refresh_settings()

    # Actions
    # -------------------------------------------------------------------------
    def on_refresh_settings(self):
        logging.error('TODO: refresh settings')

    def on_refresh(self):
        logging.debug('Refreshing lists')
        self.listMovies.clear()
        self.listMovies.setHeaderLabels(['Name', 'Year'])

        moviesWorker = MoviesWorker()
        self.connect(moviesWorker,
                     QtCore.SIGNAL('output(PyQt_PyObject)'),
                     self.addMovie)
        moviesWorker.run()

        self.listMovies.sortItems(0, QtCore.Qt.AscendingOrder)
        logging.error('TODO: refresh series list')

    def on_movieDetails(self):
        item = ''.join([unicode(self.listMovies.selectedItems()[0].text(0)),
                        unicode(self.listMovies.selectedItems()[0].text(1))])
        movie = self.movies[item]
        d = MovieDetailsDialog(movie, self)
        d.exec_()
        logging.error('TODO: implement movie details')

    def on_movieScrape(self):
        item = ''.join([unicode(self.listMovies.selectedItems()[0].text(0)),
                        unicode(self.listMovies.selectedItems()[0].text(1))])
        movie = self.movies[item]
        tmdb3.searchMovie(movie.getInfo('title'), year=movie.getInfo('year'))
        logging.error('TODO: implement scraping')

    def on_seriesDetails(self):
        logging.error('TODO: implement series details')

    # Signal handler
    # -------------------------------------------------------------------------
    def addMovie(self, movie):
        title = movie.getInfo('title')
        year = movie.getInfo('year')
        index = title + year

        self.movies[index] = movie
        self.listMovies.addTopLevelItem(QtGui.QTreeWidgetItem([title, year]))
