# -*- coding: utf-8 -*-

import logging
import os
import re
import lxml.etree as ET


class Movie(object):
    def __init__(self, path, nfofile):
        logging.debug('initializing movie in %s', path)
        self._path = path
        self._info = {
            'title': u'',
            'originaltitle': u'',
            'rating': u'',
            'year': u'',
            'votes': u'',
            'outline': u'',
            'plot': u'',
            'tagline': u'',
            'runtime': u'',
            'thumb': [],
            'fanart': [],
            'mpaa': u'',
            'id': u'',
            'genre': [],
            'country': u'',
            'set': u'',
            'credits': [],
            'director': u'',
            'premiered': u'',
            'status': u'',
            'code': u'',
            'aired': u'',
            'studio': u'',
            'trailer': u'',
            'fileinfo': {
                'streamdetails': {
                    'video': [],
                    'audio': [],
                    'subtitle': []
                }
            },
            'actor': []
        }

        if nfofile:
            if not self.getInfoFromFile(nfofile):
                self.getInfoFromName(self._path)
        else:
            self.getInfoFromName(self._path)

    def getInfoFromName(self, path):
        dirName = os.path.basename(path)
        self.setInfoString('title', re.search('(.*) \(', dirName).group(1))
        self.setInfoString('year', re.search('\((.*)\)', dirName).group(1))

    def getInfoFromFile(self, nfofile):
        """Try to read the nfofile.
           Return True if this worked and got at least title and year
           Return False otherwise"""
        try:
            tree = ET.parse(nfofile)
        except ET.ParseError:
            logging.warning('%s is not a valid xml file', nfofile)
            return False

        root = tree.getroot()
        for key in ['title', 'originaltitle', 'rating', 'year', 'votes',
                    'outline', 'plot', 'tagline', 'runtime', 'mpaa', 'id',
                    'country', 'set', 'director', 'premiered', 'status',
                    'code', 'aired', 'studio', 'trailer']:
            self.setInfoString(key, root.findtext(key))

        for key in ['genre', 'credits']:
            for i in root.findall(key):
                self.setInfoList(key, i.text)

        for thumb in root.findall('thumb'):
            self.setInfoThumb(thumb.get('preview'), thumb.text)

        fanart = root.find('fanart')
        if fanart is not None:
            for thumb in fanart.findall('thumb'):
                self.setInfoFanart(thumb.get('preview'), thumb.text)

        for actor in root.findall('actor'):
            self.setInfoActor(actor.findtext('name'), actor.findtext('role'),
                              actor.findtext('thumb'))

        fileinfo = root.find('fileinfo')
        if fileinfo is not None:
            streamdetails = fileinfo.find('streamdetails')
            for video in streamdetails.findall('video'):
                info = {}
                for key in ['codec', 'aspect', 'width', 'height',
                            'durationinseconds']:
                    info[key] = video.findtext(key)
                self.setInfoFileinfo('video', info)

            for audio in streamdetails.findall('audio'):
                info = {}
                for key in ['codec', 'language', 'channels']:
                    info[key] = audio.findtext(key)
                self.setInfoFileinfo('audio', info)

            for subtitle in streamdetails.findall('subtitle'):
                info = {}
                for key in ['language']:
                    info[key] = subtitle.findtext(key)
                self.setInfoFileinfo('subtitle', info)

        if self._info['title'] and self._info['year']:
            return True
        else:
            logging.warning('%s did not contain title and year', nfofile)
            return False

    def writeInfo(self, nfofile):
        root = ET.Element('movie')
        tree = ET.ElementTree(root)
        for key in ['title', 'originaltitle', 'rating', 'year', 'votes',
                    'outline', 'plot', 'tagline', 'runtime', 'mpaa', 'id',
                    'country', 'set', 'director', 'premiered', 'status',
                    'code', 'aired', 'studio', 'trailer']:
            e = ET.SubElement(root, key)
            e.text = self.getInfo(key)

        for key in ['genre', 'credits']:
            for item in self.getInfo(key):
                e = ET.SubElement(root, key)
                e.text = item

        for item in self.getInfo('thumb'):
            preview = item['preview']
            e = ET.SubElement(root, 'thumb', {'preview': preview})
            e.text = item['text']

        fanart = ET.SubElement(root, 'fanart')
        for item in self.getInfo('fanart'):
            preview = item['preview']
            e = ET.SubElement(fanart, 'thumb', {'preview': preview})
            e.text = item['text']

        for actor in self.getInfo('actor'):
            e = ET.SubElement(root, 'actor')
            for key in actor.keys():
                a = ET.SubElement(e, key)
                a.text = actor[key]

        fileinfo = ET.SubElement(root, 'fileinfo')
        streamdetails = ET.SubElement(fileinfo, 'streamdetails')
        for name in ['video', 'audio', 'subtitle']:
            for stream in self.getStreamdetails(name):
                e = ET.SubElement(streamdetails, name)
                for key in stream.keys():
                    i = ET.SubElement(e, key)
                    i.text = stream[key]

        with open(nfofile, 'w') as f:
            tree.write(f, pretty_print=True)

    def getInfo(self, key):
        return self._info[key]

    def getStreamdetails(self, key):
        return self._info['fileinfo']['streamdetails'][key]

    def setInfoString(self, key, value):
        if value is None:
            value = ''
        self._info[key] = unicode(value)

    def setInfoList(self, key, value):
        self._info[key].append(unicode(value))

    def setInfoThumb(self, preview, text):
        self._info['thumb'].append({'preview': unicode(preview),
                                   'text': unicode(text)})

    def setInfoFanart(self, preview, text):
        self._info['fanart'].append({'preview': unicode(preview),
                                    'text': unicode(text)})

    def setInfoActor(self, name, role, thumb):
        self._info['actor'].append({'name': unicode(name),
                                   'role': unicode(role),
                                   'thumb': unicode(thumb)})

    def setInfoFileinfo(self, key, values):
        self._info['fileinfo']['streamdetails'][key].append(values)

    def printInfo(self):
        import pprint
        pprint.PrettyPrinter().pprint(self._info)
