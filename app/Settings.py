# -*- coding: utf-8 -*-

import json
import logging
import os
from xdg import BaseDirectory

from lib import tmdb3


class Settings(object):
    def __init__(self):
        self.VERSION = '0.0'
        self.TMDBKEY = 'e3b30634468cafe1f56be309bfb571d4'
        self.configPath = os.path.join(BaseDirectory.xdg_config_home, 'pymm',
                                       'pymm.cfg')
        self.loadConfig()

        # tmdb setup
        tmdb3.set_key(self.TMDBKEY)
        tmdb3.set_locale('de', 'de', True)
        logging.error('TODO: Locale settings for TMDB not implemented')

    def loadConfig(self):
        if os.path.exists(self.configPath):
            with open(self.configPath, 'r') as f:
                self.config = json.load(f)
        else:
            logging.warning('No configuration file found, using defaults')
            self.defaultConfig()

    def saveConfig(self):
        logging.info('Saving configuration')
        logging.error('TODO: what if path doesn\'nt exist?')
        with open(self.configPath, 'w') as f:
            json.dump(self.config, f, indent=4)

    def defaultConfig(self):
        logging.debug('Resetting to default config')
        self.config = {
            'general': {
                'genres': [],
            },
            'movies': {
                'sources': [],
            },
            'series': {
                'sources': [],
            },
        }

    def addSource(self, section, path):
        if self.checkSource(section, path):
            logging.debug('Adding %s to %s', path, section)
            self.config[section]['sources'].append(unicode(path))

    def delSource(self, section, path):
        logging.debug('Removing %s from %s', path, section)
        self.config[section]['sources'].remove(unicode(path))

    def checkSource(self, section, path):
        if section not in ['movies', 'series']:
            logging.warning('%s is not a valid section', section)
            return False
        if not os.path.exists(path):
            logging.warning('%s is not a valid path', path)
            return False
        return True

    def addGenre(self, genre):
        logging.debug('Adding genre %s', genre)
        self.config['general']['genres'].append(unicode(genre))

settings = Settings()
