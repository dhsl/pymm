# -*- coding: utf-8 -*-

import fnmatch
import logging
import os
from PyQt4 import QtCore

from app.Movie import Movie
from app.Settings import settings


class MoviesWorker(QtCore.QThread):
    def __init__(self, parent=None):
        QtCore.QThread.__init__(self, parent)

    def run(self):
        for source in settings.config['movies']['sources']:
            logging.debug('scanning source %s', source)
            for root, dirs, files in os.walk(source):
                if len(dirs) == 0:
                    nfofile = None
                    for name in files:
                        if fnmatch.fnmatch(name, '*.nfo'):
                            nfofile = os.path.join(root, name)
                            logging.info('Found nfo %s', nfofile)

                    movie = Movie(root, nfofile)
                    self.emit(QtCore.SIGNAL('output(PyQt_PyObject)'), movie)
